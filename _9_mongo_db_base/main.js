var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://localhost:27017';


MongoClient.connect(url, { useNewUrlParser: true }, function (err, client) {
    if (err) {
        console.log('Невозможно подключиться к серверу MongoDB. Ошибка:', err);
    } else {
        console.log('Соединение установлено для', url);

        var db = client.db('my_database_name');
        var collection = db.collection('users');

        var user1 = {name: 'Anya', gender: 'f'};
        var user2 = {name: 'Lena', gender: 'f'};
        var user3 = {name: 'Aleksey', gender: 'm'};

        collection.insertMany([user1, user2, user3], function(err, result) {
            if (err) {
                console.log(err);
            } else {
                collection.find({gender: 'm'}).toArray(function(err, result) {
                    if (err) {
                        console.log(err);
                    } else if (result.length) {
                        console.log('Найденный:', result);
                    } else {
                        console.log('Нет документов с данным условием поиска');
                    }
                });
                // collection.deleteMany();
            }
            client.close();
        });
    }
});