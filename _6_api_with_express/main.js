const express = require("express");
const bodyParser = require("body-parser");
let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    "extended": true
}))
app.use(function(req, res, next) {
    // console.log("middleware #1");
    next();
})

//middleware, посредник
app.use(function(req, res, next) {
    // console.log("middleware #2");

    if (!req.query.token || req.query.token !== "token") {
        return res.send("permission denied");
    }

    console.log(req.query);
    next();
})

app.all("*", (req, res) => {
    console.log("endpoint");
    res.send("ok");
})

app.listen(3000, () => {
    console.log("Server start");
});