const express = require('express');
const bodyParser = require('body-parser');

const api = require('./api');

let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended': true}));

app.use((req, res, next) => {
    req.user = {user: 'Ivan'};
    next();
});

app.use('/api', api);
app.all('*', (req, res) => {
    res.send('ok');
});

app.use(function(err, req, res, next) {
    // res.json(err.stack);
    res.json({error: 'error message'});
});

app.listen(3000, () => {
    console.log('Server start');
});