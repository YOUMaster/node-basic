const express = require('express');
const app = module.exports = express();

app.use('/books', require('./books'));

app.all('*', (req, res) => {
    res.json({message: 'api version #1'});
})