const express = require('express');
const app = module.exports = express();

app.get('/:id', (req, res) => {
    res.json({book: {id: req.user}});
    // res.json({book: {id: req.params['id']}});
})