const fs = require('fs');
const conf = {
    encoding: 'utf8'
};

console.log('Читаем из файла ...');
fs.readFile('./hello.txt', conf, (err, content) => {
    if(err) return console.error(err);
    console.log('Содержимое файла:');
    console.log(content);
    console.log('Файл прочитан и содержимое выведено!');
});

////////////////////////////////////////////////////////

const text = '<?php echo "Пхп говно"; ?>';
fs.writeFile('./nichosi.php', text, err => {
    if (err) throw err;
    console.log('Файл сохранен');
});

////////////////////////////////////////////////////////

fs.open('./mnogokratnaya_zapis.php', 'w', (err, fd) => {
    if (err) {
        console.error(err);
    }
    fs.write(fd, text, (err, written) => {
        if (err) {
            console.error(err);
        }
        console.log(`В файл записано ${written} байт`);
    });
    fs.write(fd, text, (err, written) => {
        if (err) {
            console.error(err);
        }
        console.log(`В файл записано ${written} байт`);
    });
});

////////////////////////////////////////////////////////

fs.readdir('./', (err, files) => {
    console.log('Список файлов:');
    if(err) return console.error(err);
    files.forEach(file => console.log(file));
    console.log('////////////////////////////////////////////////////////');
});