const http = require("http");
const queryString = require("querystring");
const server = http.createServer();
server.listen(3000);

server.on("request", (req, res) => {
    // console.log(req.url);
    let data = "";

    req.on("data", chunk => {
        data += chunk;
    });

    req.on("end", () => {
        let parseData = parse(data, req.headers['content-type']);
        console.log(parseData);
    });

    res.end();

});

function parse(data, contentType) {
    console.log(contentType);
    if (contentType === "application/json") {
        return JSON.parse(data);
    }
    
    if (contentType === "application/x-www-form-urlencoded") {
        return queryString.parse(data);
    }

    return data;
}