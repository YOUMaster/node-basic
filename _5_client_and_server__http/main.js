const https = require("https");
const url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json";

function process(data) {
    let curr = JSON.parse(data);

    curr
        .filter(item => {
            return item.cc === "USD" || item.cc === "EUR"
        })
        .forEach(item => {
            console.log(item.txt, item.rate);
        });
}

function handler(response) {
    let data = "";
    response.on("data", function(chunk) {
        data += chunk;
    });

    response.on("end", function() {
        process(data);
    });
}

const request = https.request(url);
request.on("response", handler);
request.end();