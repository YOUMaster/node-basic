const Readable = require("stream").Readable;

/////////////////////////////////////////////

class CReadable extends Readable {
    constructor(options) {
        super(options);
        this.count = 0;
    }

    _read(size) {
        setTimeout(() => {
            this.count++;

            if (this.count === 2) {
                this.push(null);
                return;
            }

            this.push(this.count + " | " + size.toString());
        }, 1000);
    }
}

/////////////////////////////////////////////

const input = new CReadable({
    "highWaterMark": 452
});

input.on("data", console.log);
input.on("readable", function() {
    let data;

    while(data = input.read(14)) {
        console.log(data.toString());
    }
});

input.pause();