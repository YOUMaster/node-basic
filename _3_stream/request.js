"use strict";

//-------------------------------------------------------

const fs = require("fs");
const request = require("request");

//-------------------------------------------------------

request
    .get("https://google.com")
    .pipe(fs.createWriteStream("data.html"));