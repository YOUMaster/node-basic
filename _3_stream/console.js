const fs = require("fs");

const input = fs.createReadStream("data.txt");
const output = fs.createWriteStream("copy.txt");

input.pipe(output);
input.pipe(process.stdout);