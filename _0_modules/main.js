const filters = require('./filters');

const odd = [1, 2, 3, 4, 5, 6].filter(filters.odd);
console.log(odd); // [ 1, 3, 5 ]