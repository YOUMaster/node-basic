const odd = number => number % 2;
const even = number => !(number % 2);

module.exports = {
    odd,
    even
};