const express = require('express');
const bodyParser = require('body-parser');

let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended': true}));

//////////////////////////////////////////////////////

// http://example.com/api/user/12121212121-2121-21211/delete?action=438
app.get('/api/user/:userId/:action', (req, res) => {
    let params = req.params;
    let query = req.query;

    params.userId; //12121212121-2121-21211
    params.action; //delete
    query.action; //438

    res.json({
        userId: params.userId, 
        action: params.action, 
        actionId: query.action
    });
});

//////////////////////////////////////////////////////

app.all('*', (req, res) => {
    res.send('ok');
});

app.use(function(err, req, res, next) {
    // res.json(err.stack);
    res.json({error: 'error message'});
});

app.listen(3000, () => {
    console.log('Server start');
});

//schema-inspector module поискать и позырить что это