"use strict";

//---------------------------------------------------------

const EventEmitter = require("events");

//---------------------------------------------------------

class MyEmitter extends EventEmitter {}

//---------------------------------------------------------

const webApp = (function module_A() {
    const app = new MyEmitter();

    let money = 0;

    app.on("game.a.end", function(sum) {
        money += sum;
        send(money);
    });

    return app;

    function send(data) {
        console.log(data);
    }
})();

(function game_A(app) {
    setInterval(function() {
        app.emit("game.a.end", Math.random() % 2 ? 10 : 50);
    }, 2000);
})(webApp);

(function game_B(app) {
    setInterval(function() {
        app.emit("game.a.end", Math.random() % 2 ? 100 : 500);
    }, 2000);
})(webApp);